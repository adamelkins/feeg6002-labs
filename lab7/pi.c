/* pi.c for FEEG6002 lab 7
 Adam Elkins, 13 Nov 2015, ae3g10@soton.ac.uk
 */

#include<stdio.h>
/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include<stdlib.h>
#include<math.h>
#include <time.h>
clock_t startm, stopm;
#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%8.5f seconds used.", (((double) stopm-startm)/CLOCKS_PER_SEC/RUNS));
/* TIMING CODE END */

/* Variables */
#define N 10000000
/* Pi is 3.14159265358979323846264338328 to 30 decimal places --- http://www.southampton.ac.uk/~feeg6002/newlab07.html#id2 */

/* Function prototypes */
double f(double x);
double pi(long n);
double mypi(long n);

int main(void) {
    /* Declarations */



    /* Code */
    START;               /* Timing measurement starts here */
    /* Code to be written by student, calling functions from here is fine
       if desired
    */
	printf("  pi(%d) = %1.10f\n",N, pi(N));
    printf("mypi(%d) = %1.10f\n",N, mypi(N));


    STOP;                /* Timing measurement stops here */
    PRINTTIME;           /* Print timing results */
	printf("\n");
    return 0;
}

double f(double x) {
	double f = 0;
	double u = 0;
	u = pow(x,2);
	f = sqrt(1-u);
	return f;
}

double mypi(long n) {
    /* Implementation of trapeziom method for n strips */
    double pi = 0;
    int a = -1;
    int b = 1;
    long i = 0;
    double dx = ((double)b-a)/n;
    double s = 0;
    for(i=0; i<n; i++) {
        s += (f(a+i*dx)+f(a+(i+1)*dx))/2;
    }
    pi = s*dx*2;
    return pi;
}


double pi(long n) {
    /* Implementation of pseudocode from lab instructions, poor accuracy */
	double pi = 0;
	int a = -1;
	int b = 1;
	long i = 0;
	double x = 0;
	double dx = ((double)b-a)/n;
	double s = 0.5*f(a) + 0.5*f(b);
	for(i=1; i<n-1; i++) {
		x = a + i*dx;
		s = s + f(x);
	}
	pi = s*dx*2;
	return pi;
}
