/* factorial.c
 
 Implementation of LONG_MAX for FEEG6002 lab 4. Version control using git.
 Adam Elkins, 24 October 2015.
 
 email adamelkins500@gmail.com ( ae3g10@soton.ac.uk until ~30 June 2015 )
 
 */

#include <stdio.h>
#include <limits.h>
#include <math.h>


long maxlong (void) {
    /* Returns the maximum value of a long integer for use in main function. */
    return LONG_MAX;
}

double upper_bound (long n) {
    /* Computes an upper bound for n! to checl that N! can be safely computed without overflow */
    double n_bound = 1;
    
    if (n<6) {
        int i;
        for (i=n; i>1; i--) {
            n_bound *= i;
        }
    }
    else {
        n_bound = pow((n/2.), n);
    }
    return n_bound;
}

long factorial (long n) {
    /* Computes n! for n>0, returning errors if n<0 or if n! cannot be safely computed */
    long n_fact = 1;
    if (n<0) {
        n_fact = -2;
    }
    else {
        if (upper_bound(n) > LONG_MAX) {
            n_fact = -1;
        }
        else {
            int i;
            for (i=n; i>1; i--) {
                n_fact *= i;
            }
        }
    }
    return n_fact;

}

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<21; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    
    /* My own addition to test the factorial function */
    for (i=-1; i<21; i++) {
        printf("factorial(%ld)=%ld\n", i, factorial(i));
    }
    printf("upper_bound(%d)=%f\n", 20, upper_bound(20));
    printf("       long_max=%ld\n",LONG_MAX);
    printf("upper_bound(%d)=%f\n", 19, upper_bound(19));


    return 0;
}
