/* Lab8.c
 Created by Adam Elkins ae3g10@soton.ac.uk on 26/11/2015
 based on bubble_template.c obtained from http://www.southampton.ac.uk/~feeg6002/code/bubble_template.c
 */

#include<stdio.h>  /* provides standard input/output tools */
#include<stdlib.h> /* provides RAND_MAX                    */

#define N 20       /* number of random numbers             */

/* Pseudo code reads:
"""procedure bubbleSort( A : list of sortable items )
  n := length(A)-1
  for(a=0; a<= n; a++)
     for(b=n; b>a; b--)
        if A[b-1] > A[b] then
           swap (A[b-1], A[b])
        end if
     end for
  end for
end procedure
"""
*/

void swap(int *x, int *y) {
    /* Accepts two pointer objects of type int and copies the contents of memory at each pointer location to the other location. */
    int tmp = *x;   /* Temporary holding variable for contents at *x */
    *x = *y;        /* Write contents of *y to *x */
    *y = tmp;
}

/* Given an Array A of int, use bubble sort to sort elements in A (in
   place).*/
void bubble(int A[], int length) {
    /* Accepts an unsorted array A and an int defining its length; sorts the array in place (i.e. without using
     an intermiediate array to store the results). This requires the use of a swap function similar to those
     developed in previous labs. */
    int a, b, n;
    n = length - 1; /* Ignore binary zero or EOF at end of array */
    for(a=0; a<=n; a++) {
        for(b=n; b>a; b--) {
            if(A[b-1] > A[b]) {     /* Compare value of element with element below it */
                swap(&A[b-1], &A[b]); /* Swap values by sending pointers to array elements to swap() */
            }
        }
    }
}

/* Given an array of int 'a' of length 'length', print the first and
   last 'k' values */
void print_int_array(int a[], int length, int k) {
  int i;
  if (2*k < length) { /* longish array; only printing first and last k
			 values */
    for (i=0; i<k; i++)
      printf("a[%d]=%3d, ",i,a[i]);
    printf("   . . . .   ");
    for (i=length-k; i<length; i++)
      printf("a[%d]=%3d, ",i,a[i]);
  }
  else { /* for very short arrays, print all the data */
    for (i=0; i<length; i++ )
      printf("a[%d]=%3d, ",i,a[i]);
  }
  printf("\n");
}

int main(void) {
  int i;
  int data[N];
  /* initialises array with random integers between 0 and 999 */
  for (i=0; i<N; i++) {
    data[i] = (int) ((rand()+0.5)/(double) RAND_MAX * 999);
  }
  /* print data (at least beginning and end) */
  print_int_array(data,N,5);
  /* actual sorting: */
  bubble(data,N);
  printf("Data is now sorted:\n");
  /* print data (at least beginning and end) */
  print_int_array(data,N,5);

  return 0;
}
