/*  training8.c
 Created by Adam Elkins on 20/11/2015. ae3g10@soton.ac.uk
 Completed 22/11/2015
*/

#include <stdio.h>
#include <stdlib.h>

char* mix(char *s1, char *s2);
void use_mix(void);
char* make_char_array(char n);

int main(void) {
    /* Run some tests */
    printf("This is a test\n");
    use_mix();
    return 0;
}

char* mix(char *s1, char *s2) {
    int i = 0;
    int j = 0;
    char *r;
    /* Count length of input array, assume each of equal length */
    while(s1[i] != EOF && s1[i] != '\0') {
        i++;
    }
    /* Make array r and mix the two arrays */
    r = make_char_array(2*i);
    r[0] = s1[0];
    r[1] = s2[0];
    for(j=1; j<i; j++) {
        r[2*j] = s1[j];
        r[2*j+1] = s2[j];
    }
    r[2*i] = '\0';
    return r;
}

void use_mix(void) {
    /* Tests my function. Copied from http://www.southampton.ac.uk/~feeg6002/newlab08.html#training-mixing-strings */
    char s1[] = "Hello World";
    char s2[] = "1234567890!";
    
    printf("s1 = %s\n", s1);
    printf("s2 = %s\n", s2);
    printf("r  = %s\n", mix(s1, s2));
}

char* make_char_array(char n) {
    /* Copy of make_long_array from lab 7, adapted to make a char array
     Create array with n elements of type char, using dynamic memory allocation
     Return pointer to the array
     */
    
    int i=0;
    char *a;
    a = (char *)(malloc(sizeof(char)*n));
    if(a == NULL) {
        printf("Memory allocation failed\n");
        return NULL;
    }
    for(i=0; i<n; a[i++]=0);
    return a;
}
