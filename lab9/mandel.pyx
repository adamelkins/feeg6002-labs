import cython

cdef extern from "math.h":
    double sqrt(double x)
    double pow(double x, double y)

@cython.cdivision(True)
def mandel_cy(long n, long itermax=100, double xmin=-2, double xmax=0.5, double ymin=-1.25, double ymax=1.25):
    '''
    Mandelbrot fractal computation using Python.

    (n, n) are the output image dimensions
    itermax is the maximum number of iterations to do.
    xmin, xmax, ymin, ymax specify the region of the
    set to compute.

    Returns a list of lists of ints, representing the image matrix with
    n rows and n columns.
    '''

    # Let's try setting some data types:
    cdef:
        long i, it, ix, iy
        double x, y, a, b # c, z
        double complex c, z
    # create list containing n lists, each containing n zeros
    # (i.e. a matrix, represented as a list of lists)
    its = [ [0] * n for i in xrange(n)]
    # The data in the matrix are iterations, so 'its' is the plural of
    # IT for ITeration.


    # iterate through all matrix elements
    for ix in xrange(0, n):
        for iy in xrange(0, n):
            # compute the position (x, y) corresponding to matrix element
            x = xmin + ix * (xmax - xmin) / float(n)
            y = ymin + iy * (ymax - ymin) / float(n)
            # Need to count iterations
            it = 0
            # My own interpretation on the calculation of the Mandelbrot criterion.
            # Does not return correct results and a faster method has now been obtained
#            a = 0
#            b = 0
#            while it < itermax and sqrt(pow(a,2)+pow(b,2)) < 2.0:
#                a = (pow(a,2) - pow(b,2)) + x
#                b = 2*a*b + y
#                it += 1
            # c is the complex number with the given
            # x, y coordinates in the complex plane, i.e. c = x + i * y
            # where i = sqrt(-1)
            c = x + y * 1j
            z = 0
            # Here is the actual Mandelbrot criterion: we update z to be
            # z <- z^2 + c until |z| <= 2. We could the number of iterations
            # required. This number of iterations is the data we need to compute
            # (and plot if desired).
            while it < itermax and sqrt(pow(z.real,2)+pow(z.imag,2)) < 2.0:
                z = z ** 2 + c
                it += 1

            #print("ix={}, iy={}, x={}, y={}, c={}, z={}, abs(z)={}, it={}"
            #    .format(ix, iy, x, y, c, z, abs(z), it))

            # Store the result in the matrix
            its[ix][iy] = it

    return its

