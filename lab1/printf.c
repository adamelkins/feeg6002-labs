/* printf.c: prints the value of the integer i and the floating point variable x for FEEG6002
 Adam Elkins, ae3g10@soton.ac.uk, 3 Oct 2015
 */

#include<stdio.h>

int main (void) {
	int i;
	float x;
	
	i=42;
	x=3.10;
	printf("i=%d\n", i);
	printf("x=%4.2f\n",x);

	
	return 0;
	
}

