/* printfwhat.c: sets the integer i to the length of the string "What am I doing?/n" which treats the newline as one character, for FEEG6002
 Adam Elkins, ae3g10@soton.ac.uk, 3 Oct 2015
 */

#include<stdio.h>

int main (void) {
	int i;
	
	i=printf("What am I doing?\n");
	printf("i=%d\n",i); /*prints the number of characters in the string, including newline character */

	
	return 0;
}

