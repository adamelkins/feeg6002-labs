{
"cells": [
 {
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "<h1 align=\"center\"><font color=\"0066FF\" size=110>Eigenvalue problems II: 1D Helmholtz Equation</font></h1>\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "import numpy as np\n",
  "import scipy as sp\n",
  "import scipy.linalg\n",
  "import matplotlib\n",
  "from IPython.html.widgets import interact\n",
  "from IPython.display import Image, YouTubeVideo\n",
  "try:\n",
  "    %matplotlib inline\n",
  "except:\n",
  "    # not in notebook\n",
  "    pass\n",
  "LECTURE = False\n",
  "if LECTURE:\n",
  "    size = 20\n",
  "    matplotlib.rcParams['figure.figsize'] = (10, 6)\n",
  "    matplotlib.rcParams['axes.labelsize'] = size\n",
  "    matplotlib.rcParams['axes.titlesize'] = size\n",
  "    matplotlib.rcParams['xtick.labelsize'] = size * 0.6\n",
  "    matplotlib.rcParams['ytick.labelsize'] = size * 0.6\n",
  "import matplotlib.pyplot as plt\n",
  "\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "# Learning Outcomes\n",
  "\n",
  "By the end of this lecture, you should be able to\n",
  "-   Transform the 1D wave equation into the 1D Helmholtz equation.\n",
  "-   Discretise the Helmholtz equation using the finite difference method to obtain an eigenvalue problem.\n",
  "-   Use the Jacobi eigenvalue method to solve the eigenvalue problem and plot the first few modes / eigenvectors.\n",
  "\n",
  "# Introduction\n",
  "\n",
  "The previous lecture introduced eigenvalue problems, in which given a matrix $A$, we seek the eigenvalues $\\lambda$ such and the (non-zero) eigenvectors $x$ such that\n",
  "\n",
  "\\begin{equation}\n",
  "A x = \\lambda x.\n",
  "\\end{equation}\n",
  "\n",
  "We showed how the Jacobi eigenvalue method can be used to obtaining the eigenvalues and eigenvectors when $A$ is a symmetric matrix. The Jacobi method is an iterative method that uses rotation matrices, or so-called Jacobi or Givens matrices, to transform $A$ into a diagonal matrix.\n",
  "\n",
  "In this lecture, we will show how some Partial Differential Equations lead to eigenvalue problems by solving the 1D Helmholtz equation.\n",
  "\n",
  "# 1D Helmholtz equation: the vibrating string\n",
  "\n",
  "## Partial Differential Equation\n",
  "\n",
  "![img](fig09-01.png \"Vertical displacement $u(x, t)$ of a string of length $L$ clamped at both ends.\")\n",
  "\n",
  "Consider a guitar string clamped at both ends. When the string is displaced vertically by a small amount and then released, its displacement $u(x, t)$ satisfies the wave equation (see [Wikipedia's article on the vibrating string](https://en.wikipedia.org/wiki/Vibrating_string) for a derivation based on Newton's second law):\n",
  "\n",
  "\\begin{equation}\n",
  "\\frac{\\partial^2 }{\\partial x^2} u(x, t) - \\frac{1}{c^2}\\frac{\\partial^2}{\\partial t^2} u(x, t)  = 0,\n",
  "\\end{equation}\n",
  "\n",
  "where $c$ is a constant that depends on the material of the string and on its tension. As we discussed in [lecture 07](http://www.soton.ac.uk/~feeg6002/lecturenotes/feeg6002_numerical_methods07.html) (c.f. hyperbolic equations section), this equation indicates that $u$ takes the form of waves propagating along the string at speed $c$.\n",
  "\n",
  "This equation is hyperbolic and somewhat challenging to solve, but if we express the displacement in the form\n",
  "\n",
  "\\begin{equation}\n",
  "u(x, t) = \\text{Re}\\left(U(x) e^{i \\omega t}\\right) = U(x) \\cos (\\omega t)\n",
  "\\end{equation}\n",
  "\n",
  "where $U$ is a real function of $x$, and $\\omega$ is an arbitrary frequency,  then the wave equation reduces to\n",
  "\n",
  "\\begin{equation}\n",
  "U''(x) + k^2 U(x)  = 0,\n",
  "\\end{equation}\n",
  "\n",
  "where $k = \\omega / c$ is called the wavenumber. We have used the *separation of variable* technique which is a powerful way of solving PDEs.\n",
  "\n",
  "This equation is called the Helmholtz equation and it is parabolic. Thus, for every frequency $\\omega$, there is a function $U(x)$ such that the displacement $u(x, t)$ satisfies the wave equation. It seems that $\\omega$ must be related to an eigenvalue of the system, and that $U(x)$ must be the corresponding eigenvector. Let's demonstrate that this is indeed the case by discretising the Helmholtz equation using the Finite Difference Method.\n",
  "\n",
  "## Difference equation\n",
  "\n",
  "As usual, let's start with only a few unkowns. Figure 2 represents the string using 5 uniformly spaced nodes, with 2 nodes at the left and right boundaries where the displacements are 0, and 3 nodes in between, $U_0$, $U_1$ and $U_2$, where the displacement in unkown.\n",
  "\n",
  "![img](fig09-02.png \"Vertical displacement $U(x)$ over a uniform mesh of guitar string of length $L$ clamped at both ends, with unknown 3 mesh points.\")\n",
  "\n",
  "The term $U''$ is our familiar Laplace operator in 1D, which we first discretized in [lecture 04](http://www.soton.ac.uk/~feeg6002/lecturenotes/feeg6002_numerical_methods04.html) using the first central approximation for the second derivative:\n",
  "\n",
  "\\begin{equation}\n",
  "U''(x) = \\frac{U(x - h) - 2 U(x) + U(x + h)}{h^2} + O(h^2).\n",
  "\\end{equation}\n",
  "\n",
  "This leads to the following difference equation, accurate to order 2:\n",
  "\n",
  "\\begin{equation}\n",
  "U(x - h) - 2 U(x) + U(x + h) + h^2 k^2 U(x) = 0,\n",
  "\\end{equation}\n",
  "\n",
  "which can be put in the form\n",
  "\n",
  "\\begin{align}\n",
  "&-U(x - h) + 2 U(x) - U(x + h) = \\lambda U(x), & \\text{where} \\quad \\lambda &\\equiv h^2 k^2.\n",
  "\\end{align}\n",
  "\n",
  "If we can solve the above equation for $U$ and $\\lambda$, then we can recover the wavenumber from $k = \\sqrt{\\lambda} / h$.\n",
  "\n",
  "## Discretized equation\n",
  "\n",
  "We can now use our difference equation for each (inner) node in our domain to obtain a system of equations, which we will put in matrix form. For our three unkowns $U_0$, $U_1$ and $U_2$, we have\n",
  "\n",
  "\\begin{equation}\n",
  "\\left\\{\n",
  "\\begin{aligned}\n",
  "-0 + 2 U_0 - U_1 = \\lambda U_0, \\\\\n",
  "-U_0 + 2 U_1 - U_2 = \\lambda U_1, \\\\\n",
  "-U_1 + 2 U_2 - 0 = \\lambda U_2,\n",
  "\\end{aligned}\\right.\n",
  "\\end{equation}\n",
  "\n",
  "\\begin{equation}\n",
  "\\begin{pmatrix}\n",
  "2 & -1 & 0 \\\\\n",
  "-1 & 2 & -1 \\\\\n",
  "0 & -1 & 2\n",
  "\\end{pmatrix}\n",
  "\\begin{pmatrix}\n",
  "U_0 \\\\\n",
  "U_1 \\\\\n",
  "U_2\n",
  "\\end{pmatrix}\n",
  "=\n",
  "\\lambda\n",
  "\\begin{pmatrix}\n",
  "U_0 \\\\\n",
  "U_1 \\\\\n",
  "U_2\n",
  "\\end{pmatrix}.\n",
  "\\end{equation}\n",
  "\n",
  "As expected, this is an eigenvalue problem and each eigenvalue / eigenvector pair gives us a solution to the Helmholtz equation, from which we can retrieve a solution to the original wave equation. This solution is called an acoustic mode. For a string with $N$ degrees of freedom, you can find $N$ modes. The physical solution would be a linear combination of all these modes.\n",
  "\n",
  "We recognise our tri-diagonal Laplacian matrix, with 2s on the diagonal and -1s on the first upper and lower diagonals. So for a string with $N$ degrees of freedom, the problem would look like\n",
  "\n",
  "\\begin{equation}\n",
  "\\begin{pmatrix}\n",
  " 2 & -1 &   &  &  &  \\\\\n",
  " -1 & 2 & -1 &  &  &  \\\\\n",
  " & \\ddots & \\ddots & \\ddots &    &   \\\\\n",
  " &  & \\ddots & \\ddots & \\ddots &  \\\\\n",
  " &   &   & -1 &  2 & -1 \\\\\n",
  " &   &   &    & -1 &  2 \\\\\n",
  "\\end{pmatrix}\n",
  "\\begin{pmatrix}\n",
  "U_0 \\\\ U_1 \\\\ \\vdots \\\\ \\vdots \\\\ U_{N-2} \\\\ U_{N-1}\n",
  "\\end{pmatrix}\n",
  "=\n",
  "\\lambda\n",
  "\\begin{pmatrix}\n",
  "U_0 \\\\ U_1 \\\\ \\vdots \\\\ \\vdots \\\\ U_{N-2} \\\\ U_{N-1}\n",
  "\\end{pmatrix}\n",
  "\\end{equation}\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "def get_A(N):\n",
  "    \"\"\"Return our 2nd order Laplacian matrix\"\"\"\n",
  "    offset = 1\n",
  "    upper = -np.diag(np.ones(N - 1), offset)\n",
  "    lower = -np.diag(np.ones(N - 1), -offset)\n",
  "    diag = 2 * np.diag(np.ones(N))\n",
  "    A = diag + upper + lower\n",
  "    return A\n",
  "\n",
  "N = 10\n",
  "A = get_A(N)\n",
  "print 'A = \\n', A\n",
  "\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "## Numerical Solution\n",
  "\n",
  "What do these modes look like? Let's solve the above eigenvalue problem. Since the matrix is symmetric, we can use our Jacobi code from [lecture 08](http://www.soton.ac.uk/~feeg6002/lecturenotes/feeg6002_numerical_methods08.html), which you can download from snippets tab on the course website ([lecture08.py](http://www.soton.ac.uk/~feeg6002/snippets/numerical_methods/lecture08.py)). Here we will implement an additional function `jacobi_eig_sorted` that sorts the eigenvalues from small to large and re-orders the columns in the matrix of eigenvectors accordingly. This is easily done using the NumPy's `argsort` array method.\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "from lecture08 import jacobi_eig\n",
  "def jacobi_eig_sorted(a, tol=1e-9):\n",
  "    \"\"\"Solve the eigenvalue problem aw = v.w and return sorted\n",
  "    eigenvalues v and eigenvectors w.\"\"\"\n",
  "    eigvals, eigvecs = jacobi_eig(a, tol)\n",
  "    idx = eigvals.argsort()\n",
  "    s_eigvals = eigvals[idx]  # sorted\n",
  "    s_eigvecs = eigvecs[:,idx] # sorted\n",
  "    return s_eigvals, s_eigvecs\n",
  "\n",
  "eigvals, U = jacobi_eig(A)\n",
  "# eigenvalues are in vector eigvals and the eigenvectors are the columns of U\n",
  "mode = 0\n",
  "print 'eigenvalue #%d: \\n\\t' % mode, eigvals[mode]\n",
  "print 'eigenvector #%d: \\n\\t' % mode, U[:, mode]\n",
  "\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "We can use an embedding function as in [lecture 06](http://www.soton.ac.uk/~feeg6002/lecturenotes/feeg6002_numerical_methods06.html) to add the boundary points where the displacement is 0.\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "def embed(u):\n",
  "    \"\"\"Add a 0 at the start and end of the array\"\"\"\n",
  "    return np.concatenate([(0,), u, (0,)])\n",
  "# Test\n",
  "print embed([1,2,3])\n",
  "\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "Let's compute the matrix $A$ and its eigenvalues, and plot the vertical displacement of our string for various modes:\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "N = 50\n",
  "L = 1.0\n",
  "A = get_A(N)\n",
  "eigvals, eigvects = jacobi_eig_sorted(A)\n",
  "\n",
  "def plot_mode(mode=0, xlabel=False):\n",
  "    lbda = eigvals[mode]\n",
  "    U = embed(eigvects[:, mode])\n",
  "    h = 1.0 / (N + 1.0)  # beware of the indivision bug\n",
  "    k = np.sqrt(lbda) / h\n",
  "\n",
  "    x = np.linspace(0, 1.0, N + 2)  # x / L\n",
  "    plt.plot(x, U/max(U))\n",
  "    plt.ylim([-1.01, 1.01])\n",
  "    if xlabel:\n",
  "        plt.xlabel(r'$x/L$')\n",
  "# Test\n",
  "if LECTURE:\n",
  "    plot_mode(mode=1)\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "plt.figure(1)\n",
  "plt.clf()\n",
  "\n",
  "if LECTURE:\n",
  "    i = interact(plot_mode, mode=(0, 10))\n",
  "else:\n",
  "    # Plot the first few modes\n",
  "    nbmodes = 5\n",
  "    fig, axes = plt.subplots(5,1, sharex=True, figsize=(8, 8))\n",
  "    for mode in range(nbmodes):\n",
  "        plt.sca(axes[mode]) # set the current axis to 'ax'\n",
  "        plot_mode(mode)\n",
  "        plt.ylabel('m=%d' % mode)\n",
  "    plt.xlabel(r'$x/L$')\n",
  "    plt.savefig('fig09-02.pdf')\n",
  "\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "<fig09-01.pdf>\n",
  "\n",
  "## Interpretation\n",
  "\n",
  "Using the above approach, we find the discrete list of eigenvalues $(\\lambda_i)$ (and eigenvectors)  for our string. Since $\\omega/c = k =  \\sqrt{\\lambda} / h$, this translates into a finite number of frequencies $(\\omega_i)$. These are precisely the natural frequencies that we discussed in the previous lecture.\n",
  "\n",
  "If the string is excited at one of these natural frequencies $\\omega_i$, it will vibrate with the shape defined by the associated eigenvector $U_{\\omega_i}(x)$, or, in the time domain, $u(x, t) = U_{\\omega_i} \\cos(\\omega_i t)$. This is beautifully illustrated in [this YouTube video](https://www.youtube.com/watch?v%3D-gr7KmTOrx0).\n",
  "\n"
  ]
 },
{
 "cell_type": "code",
 "metadata": {},
  "outputs": [],
 "execution_count": 1,

 "source": [
  "if LECTURE:\n",
  "    YouTubeVideo('-gr7KmTOrx0')\n",
  "\n"
  ]
 },
{
 "cell_type": "markdown",
 "metadata": {},
  
 "source": [
  "## Self Study\n",
  "\n",
  "1.  You can animate the mode shapes by plotting the $u(x, t)$ instead of $U(x)$ for a given eigenvector, to reproduce the standing waves illustrated in the above video. Add a keyword parameter `omega_t=0` to the `plot_mode` function and use it to plot the displacement $u(x, t)$ along the string as a function of $x$, for a fixed non-dimensional time `omega_t`. Modify the call to interact in the above cell to visualise the evolution of the mode shape with time over a period ($0 \\leq \\omega t \\leq 2\\pi$).\n",
  "\n",
  "2\\*. (Hard) Think about how you could generalise this lecture to 2D or 3D. In 2D a similar problem would be to obtain the acoustic modes in a rectangular or a cylindrical duct. Have a look at [my MSc dissertation](http://www.southampton.ac.uk/~ss53g10/data/conference-papers/SinayokoJosephMcAlpine2008_AIAA.pdf) for an analytical solution to the cylindrical problem, which you can use to validate your code.\n",
  "\n",
  "# Conclusions\n",
  "\n",
  "-   Use a Fourier expansion of the form $u(x,t) = U(x) \\cos(\\omega t)$ to transform the wave equation into a Helmholtz equation.\n",
  "-   Use the 2nd order Laplacian stencil to discretise the Helmholtz equation into an eigenvalue problem.\n",
  "-   Use the Jacobi matrix to solve the problem and plot the mode shapes.\n",
  "\n",
  "Next week we will look at alternatives to the Jacobi eigenvalue method for solving eigenvalue problems.\n",
  "\n"
  ]
 }
],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}