/* interest.c 

 */

#include <stdio.h>

int main(void) {
    int s=1000;
    float rate = 0.03;
    int month;
    float debt = s;
    float interest;
    float totalinterest = 0;
    float frac;
    
    for (month=1; month<25; month++) {
        interest = debt * rate;
        debt = debt + interest;
        totalinterest = totalinterest + interest;
        frac = 100*totalinterest/s;
        printf("month %2d: debt=%7.2f, interest=%5.2f, total_interest=%7.2f, frac=%6.2f%%\n", month, debt, interest, totalinterest, frac);
    }
    return 0;
}
