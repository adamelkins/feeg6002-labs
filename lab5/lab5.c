#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
void reverse(char source[], char target[]);
long string_length(char s[]);

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

/* reverse the order of characters in 'source', write to 'target'.
   Assume 'target' is big enough. */
void reverse(char source[], char target[]) {
    int c = 0;
    int d = 0;
    long length = string_length(source);
/*    printf("length = %ld\n",length);  --- testing */
    while (c < length ) {  /* source[c] != EOF && source[c] != '\0' --- old code */
        d = length - c - 1;
        target[d] = source[c];
/*        printf("c = %d, source[c] = %c, d = %d\n", c, source[c], d); --- testing code */
        c++;
    }
    target[length] = '\0'; /* Append string terminator to end of reversed string */
}

long string_length(char s[]) {
    long c = 0;
    while (s[c] != EOF && s[c] != '\0') { /* counts the characters in a string */
        c++;
    }
    return c;
}
