#include <stdio.h>

/* function prototype */
long string_length(char s[]);

int main(void) {
/*    char c = getchar();
    long d = string_length(c);
    printf("%ld",d);
    
    */
  char s1[]="Hello";
  char s2[]="x";
  char s3[]="line 1\tline 2\n";

  printf("%20s | %s\n", "string_length(s)", "s");
  printf("%20ld | %s\n", string_length(s1), s1);
  printf("%20ld | %s\n", string_length(s2), s2);
  printf("%20ld | %s\n", string_length(s3), s3);
    
  return 0;
}

long string_length(char s[]) {
    long c = 0;
    while (s[c] != EOF && s[c] != '\0') {
        /* printf("s = %c c = %ld\n",s[c],c);  Line to help solve error in output */
        c++;
    }
    return c;
}
