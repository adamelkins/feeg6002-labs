/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/


/* Function prototypes */
void reverse(char source[], char target[]);
long string_length(char s[]);
void rstrip(char s[]);
void lstrip(char s[]);

int main(void) {
  char test1[] = "\t Hello World   ";

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);
    lstrip(test1);
    printf("l-stripped string reads: |%s|\n", test1);

  return 0;
}

void rstrip(char s[]) {
    /* to be implemented */
    /* First reverse string, placing terminator correctly, then use lstrip, then reverse string again.
     */
    int i = 0;
    char target[1000]; /*char target[string_length(s)];*/  /* Remove variable length array warning */
    while(target[i] != EOF && target[i] != '\0') {
        target[i] = 0;
        i++;
    }
    reverse(s, target);
    lstrip(target);
    reverse(target, s);
}

void lstrip(char s[]) {
    int i = 0;
    int k = 0;
    while(s[i] != EOF && s[i] != '\0') {
        if(s[i] != ' ' && s[i] != '\t') {
            while(s[i] != EOF && s[i] != '\0') {
                s[k] = s[i];
                k++;
                i++;
            }
        }
        else {
            i++;
        }
        s[k] = '\0';
    }
}

void reverse(char source[], char target[]) {
    int c = 0;
    int d = 0;
    long length = string_length(source);
    while (c < length ) {
        d = length - c - 1;
        target[d] = source[c];
        c++;
    }
    target[length] = '\0'; /* Append string terminator to end of reversed string */
}

long string_length(char s[]) {
    long c = 0;
    while (s[c] != EOF && s[c] != '\0') { /* counts the characters in a string */
        c++;
    }
    return c;
}
