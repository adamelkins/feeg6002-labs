/*  celsius.c
	
 */

#include<stdio.h>

int main(void) {
	
	int C;
	float Fahrenheit;
	
	for (C=-30; C<31; C=C+2) {
		Fahrenheit = C * 9/5. + 32;
		printf("%3d = %5.1f\n", C, Fahrenheit);
	}
	getchar();
	return 0;
}

