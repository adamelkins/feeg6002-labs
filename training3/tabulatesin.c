/* Print a table of the values of sin x for certain values of x, for FEEG6002. Demonstrates use of #define symbols in for loops.
    Adam Elkins, 16 October 2015, ae3g10@soton.ac.uk
 */

#include <stdio.h>
#include <math.h>

#define N 10
#define XMIN 1.0
#define XMAX 10.0

int main(void) {
    double x;
    double y;
    int i;
    for (i=0; i<N; i++) {
        x=XMIN + (XMAX - XMIN) / (N-1) * i;
        y=sin(x);
        printf("%f %f\n", x, y);
    }
    return 0;
}
