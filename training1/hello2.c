/* hello2.c: Modified version of hello.c, gets and returns the character typed at the prompt
 Adam Elkins, ae3g10@soton.ac.uk, 3 Oct 2015
*/

#include<stdio.h>

char a;

int main (void) {
	printf("Hello World!\n");
    a=getchar();
    printf("The character is %c\n",a);
    getchar();
	return 0;
	
}

