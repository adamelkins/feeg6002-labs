/* hello.c: introduction to c. Prints the string "Hello World!" followed by a newline, for FEEG6002
 Adam Elkins, ae3g10@soton.ac.uk, 3 Oct 2015
*/

#include<stdio.h>

int main (void) {
	printf("Hello World!\n");
    getchar();
	return 0;
	
}

